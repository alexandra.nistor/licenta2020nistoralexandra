DATA_PATH = './DATA/'

INPUT_CSV = './DATA/csv files/oa_file_list.csv'
# consists of (File, Article Citation, Accession ID, Last Updated (YYYY-MM-DD HH:MM:SS), PMID, License)
INPUT_CSV_FIELD_CITATION = 2
INPUT_CSV_FIELD_PMID = 5

NUMBER_OF_ABSTRACTS = 600

# ABSTRACTS_CSV
# consists of (PMID, Abstract)
ABSTRACTS_CSV = './DATA/csv files/abstracts_list.csv'
ABSTRACTS_CSV_FIELD_PMID = 1
ABSTRACTS_CSV_FIELD_ABSTRACT = 2
ABSTRACTS_CSV_FIELD_1 = 'PMID'
ABSTRACTS_CSV_FIELD_2 = 'Abstract'

# CONCEPTS_CSV
# consists of (PMID, Concept)
CONCEPTS_CSV = './DATA/csv files/concepts_list.csv'
CONCEPTS_CSV_FIELD_PMID = 1
CONCEPTS_CSV_FIELD_CONCEPT = 2
CONCEPTS_CSV_FIELD_1 = 'PMID'
CONCEPTS_CSV_FIELD_2 = 'Concept'

METAMAP_PATH = '/home/ada/LICENTA/public_mm/bin/metamap18'

DS_ALL_CONCEPTS = './DATA/common data structures/all_concepts.npy'
DS_UNIQUE_CONCEPTS = './DATA/common data structures/unique_concepts.npy'
DS_PMIDS = './DATA/common data structures/pmids.npy'
DS_DICT = './DATA/common data structures/pmid_concepts_dict.json'
DS_CORPUS = './DATA/common data structures/corpus.npy'

WORD2VEC_MODEL_1 = './DATA/word2vec_1.model'
# with (size=100, window=5, min_count=5, sg=1) and epochs=10

WORD2VEC_MODEL_2 = './DATA/word2vec_2.model'
# with (size=100, window=5, min_count=1, sg=1) and epochs=15

ASSOCIATION_MATRIX = './DATA/association_matrix.npy'
WEIGHTING_SCHEME_1 = './DATA/ws.npy'
