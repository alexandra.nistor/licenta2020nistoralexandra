import constants
import csv
import numpy as np
import re


def clean_corpus(abstracts_csv, pmids):
    """
    The corpus will contain abstracts with lower alphabetical characters only.
    :param abstracts_csv.
    :param pmids: list of pmids, corresponding to the abtracts which will be cleaned.
    :return: list of clean abstracts.
    """
    fd = open(abstracts_csv, 'r', encoding='utf-8')
    reader = csv.reader(fd, delimiter=',')
    next(reader)

    corpus = []
    for row in reader:
        if row[constants.ABSTRACTS_CSV_FIELD_PMID - 1] in pmids:
            abstract = row[constants.ABSTRACTS_CSV_FIELD_ABSTRACT - 1]
            abstract = re.sub('[^a-zA-Z]', ' ', abstract).lower()
            abstract = re.sub(r'\s+', ' ', abstract)
            corpus.append(abstract)
    fd.close()
    return corpus


def execute():
    pmids = list(np.load(constants.DS_PMIDS))
    corpus = clean_corpus(constants.ABSTRACTS_CSV, pmids)
    np.save(constants.DS_CORPUS, corpus)


execute()
