import constants
import numpy as np
from gensim.models import Word2Vec
from sklearn.metrics.pairwise import cosine_similarity

word2vec_model = Word2Vec.load(constants.WORD2VEC_MODEL_2)


def build_vectors(unique_concepts):
    """
    Builds a vector for each unique concept.
    If the concept contains more than one word, it computes aggregated word embedding.
    :param unique_concepts.
    :return: list of vectors of all unique concepts.
    """
    vectors = []
    for concept in unique_concepts:
        words = concept.split(' ')
        len_concept = len(words)
        if len_concept >= 2:
            vector = word2vec_model.wv[words[0]]
            for i in range(1, len_concept):
                vector = np.add(vector, word2vec_model.wv[words[i]])
        else:
            vector = word2vec_model.wv[concept]
        vectors.append(vector)
    return vectors


def build_association_scores_matrix(vectors, n):
    """
    Builds a matrix of association scores (Cosine distance) between all unique concepts.
    :param vectors: list of vectors of all unique concepts.
    :param n: number of unique concepts.
    :return: numpy.ndarray.
    """
    # length of a single vector
    len_vector = len(vectors[0])

    # reshaped vectors for computing cosine similarity
    vectors_reshaped = list(map(lambda v: v.reshape(1, len_vector), vectors))

    s = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            vi = vectors_reshaped[i]
            vj = vectors_reshaped[j]
            s[i][j] = cosine_similarity(vi, vj)[0][0]
    return s


def execute():
    unique_concepts = list(np.load(constants.DS_UNIQUE_CONCEPTS))
    n = len(unique_concepts)

    vectors = build_vectors(unique_concepts)
    # s = build_association_scores_matrix(vectors, n)
    # np.save(constants.ASSOCIATION_MATRIX, s)


execute()
