import constants
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from sklearn.metrics import davies_bouldin_score
from sklearn.decomposition import TruncatedSVD
from scipy.spatial.distance import cdist

ws = np.load(constants.WEIGHTING_SCHEME_1)
k = 6


def k_means():
    km = KMeans(n_clusters=k, init='k-means++', max_iter=100, n_init=5, verbose=1)
    km.fit(ws)
    return km


def t_sne():
    print(ws.shape)
    ws_reduced = TruncatedSVD(n_components=k, random_state=0).fit_transform(ws)
    print(ws_reduced)
    ws_embedded = TSNE(n_components=2, perplexity=40, verbose=2).fit_transform(ws_reduced)

    km = k_means()
    plt.scatter(ws_embedded[:, 0], ws_embedded[:, 1], marker='o', c=km.labels_, alpha=0.7, s=100, cmap='tab10')
    plt.show()
    print(davies_bouldin_score(ws, km.labels_))


def execute():
    t_sne()


execute()
