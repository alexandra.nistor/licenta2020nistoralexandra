import constants
import csv
import pandas as pd
from pymetamap import MetaMap

# MetaMap parameter number - ConceptMMI
semtypes = 5
trigger = 6
# MetaMap parameter number - trigger - ConceptMMI
text = 3

mm = MetaMap.get_instance(constants.METAMAP_PATH)


def process_abstracts_csv(abstracts_csv):
    """
    :param abstracts_csv.
    :return: tuple - (list of pmids, list of abstracts).
    """
    fd = open(abstracts_csv, 'r')
    reader = csv.reader(fd, delimiter=',')
    next(reader)

    pmids = []
    abstracts = []
    for row in reader:
        pmids.append(row[constants.ABSTRACTS_CSV_FIELD_PMID - 1])
        abstracts.append(row[constants.ABSTRACTS_CSV_FIELD_ABSTRACT - 1])
    fd.close()
    return pmids, abstracts


def get_diseases_first_abstract(abstracts_csv):
    """
    Gets the diseases of the first abstract.
    :param abstracts_csv.
    :return: list of diseases.
    """
    abstract = process_abstracts_csv(abstracts_csv)[1][10]
    diseases = []
    concepts, errors = mm.extract_concepts([abstract])
    for concept in concepts:
        if concept[semtypes] == '[diap]' or concept[semtypes] == '[neop]':
            disease = concept[trigger].split('-')[text]
            disease = disease.lower().strip('"')
            diseases.append(disease)
    return diseases


def create_concepts_csv(concepts_csv, abstracts_csv):
    """
    Creates a .csv file with concepts of type 'diap' or 'neop' and their corresponding pmid.
    :param concepts_csv.
    :param abstracts_csv.
    """
    # TODO log time of creating this file
    fd = open(concepts_csv, 'a')
    writer = csv.writer(fd, delimiter=',')
    writer.writerow([constants.CONCEPTS_CSV_FIELD_PMID, constants.CONCEPTS_CSV_FIELD_CONCEPT])

    pmids, abstracts = process_abstracts_csv(abstracts_csv)
    for i in range(len(pmids)):
        concepts, errors = mm.extract_concepts([abstracts[i]])
        for concept in concepts:
            if concept[semtypes] == '[diap]' or concept[semtypes] == '[neop]':
                disease = concept[trigger].split('-')[text]
                disease = disease.lower().strip('"')
                writer.writerow([pmids[i], disease])
    fd.close()

    # removes duplicate rows
    df = pd.read_csv(concepts_csv, sep=',')
    df.drop_duplicates(subset=None, inplace=True)
    df.to_csv(concepts_csv, index=False)


def execute():
    # print(get_diseases_first_abstract(ABSTRACTS_CSV))
    create_concepts_csv(constants.CONCEPTS_CSV, constants.ABSTRACTS_CSV)


execute()
