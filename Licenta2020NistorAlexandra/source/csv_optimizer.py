import pandas as pd

CONCEPTS_CSV = './DATA/concepts_list.csv'


def optimize_csv(concepts_csv):
    """
    Removes duplicate rows of a .csv file.
    :param concepts_csv.
    """
    df = pd.read_csv(concepts_csv, sep=',')
    df.drop_duplicates(subset=None, inplace=True)
    df.to_csv(concepts_csv, index=False)


def execute():
    optimize_csv(CONCEPTS_CSV)


execute()
