import constants
import numpy as np

s = np.load(constants.ASSOCIATION_MATRIX)
unique_concepts = list(np.load(constants.DS_UNIQUE_CONCEPTS))
n = len(unique_concepts)


def get_similar_concepts(target_concept, top):
    """
    Returns a list of top most similar concepts, sorted descending by the association scores.
    :param target_concept.
    :param top: number of the most similar concepts.
    :return: list of tuples - (concept, association score).
    """
    try:
        i = unique_concepts.index(target_concept)
    except:
        return 'Concept "{}" is not on the list.'.format(target_concept)

    assoc_scores = [s[i][j] for j in range(n)]
    assoc_scores.remove(s[i][i])
    sorted_assoc_scores = sorted(assoc_scores)[-top:]
    similar_concepts = []
    for score in sorted_assoc_scores:
        i = assoc_scores.index(score)
        similar_concepts.append((unique_concepts[i], score))

    # sort based on the association scores
    similar_concepts = sorted(similar_concepts, reverse=True, key=lambda x: x[1])
    return similar_concepts


print(get_similar_concepts('molecular imaging techniques', 5))
