import constants
import nltk
import numpy as np
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer

data_path = constants.DATA_PATH


def stem_words(words):
    return [PorterStemmer().stem(w) for w in words]


def tokenize_corpus(corpus):
    tokens = nltk.word_tokenize(corpus)
    stems = stem_words(tokens)
    return stems


def save_sparse_csr_matrix(filename, matrix):
    np.savez(filename, data=matrix.data, indices=matrix.indices, indptr=matrix.indptr, shape=matrix.shape)


def execute():
    corpus = list(np.load(constants.DS_CORPUS))
    tfidf_vectorizer = TfidfVectorizer(tokenizer=tokenize_corpus(corpus))
    tfs2 = tfidf_vectorizer.fit_transform(corpus)
    save_sparse_csr_matrix(data_path + 'concepts_tf_idf_2.npz', tfs2)


execute()
