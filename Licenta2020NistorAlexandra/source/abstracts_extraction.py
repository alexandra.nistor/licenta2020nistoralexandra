import constants
import csv
import random
from Bio import Entrez

Entrez.email = 'adanistor12@gmail.com'


def get_pmids(input_csv):
    """
    :param input_csv.
    :return: list of all pmids whose journal abbreviation starts with 'A' or 'B'.
    """
    fd = open(input_csv, 'r')
    reader = csv.reader(fd, delimiter=',')
    next(reader)

    pmids = []
    for row in reader:
        journal_abbr = row[constants.INPUT_CSV_FIELD_CITATION - 1].split('.')[0]
        if journal_abbr.startswith('A') or journal_abbr.startswith('B'):
            pmid = row[constants.INPUT_CSV_FIELD_PMID - 1]
            if pmid:
                pmids.append(pmid)
    fd.close()

    # randomly selects NUMBER_OF_ABSTRACTS PMIDs
    random.seed(10)
    pmids = random.sample(pmids, constants.NUMBER_OF_ABSTRACTS)
    return pmids


# TODO statistic with number of abstracts for each journal abbreviation


def create_abstracts_file(abtracts_csv, pmids):
    """
    Creates a .csv file with each pmid and its corresponding abstract.
    :param abtracts_csv.
    :param pmids.
    """
    fd = open(abtracts_csv, 'a', newline='', encoding='utf-8')
    writer = csv.writer(fd, delimiter=',')
    writer.writerow([constants.ABSTRACTS_CSV_FIELD_1, constants.ABSTRACTS_CSV_FIELD_2])

    handle = Entrez.efetch(db='pubmed', id=pmids, rettype='xml', retmode='text')
    records = Entrez.read(handle)

    for article in records['PubmedArticle']:
        if 'Abstract' in article['MedlineCitation']['Article'].keys():
            pmid = article['MedlineCitation']['PMID']
            abstract = ''
            for text in article['MedlineCitation']['Article']['Abstract']['AbstractText']:
                abstract += str(text)
            writer.writerow([pmid, abstract])
    fd.close()


def execute():
    pmids = get_pmids(constants.INPUT_CSV)
    create_abstracts_file(constants.ABSTRACTS_CSV, pmids)


execute()
