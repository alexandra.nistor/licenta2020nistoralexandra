import matplotlib.pyplot as plt
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from sklearn.metrics import davies_bouldin_score
from sklearn.decomposition import TruncatedSVD

CSR_MATRIX_PATH_1 = './DATA/concepts_tf_idf_1.npz'
CSR_MATRIX_PATH = './DATA/concepts_tf_idf_2.npz'


def load_sparse_matrix_csr(filename):
    loader = np.load(filename)
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']), shape=loader['shape'])


tfs = load_sparse_matrix_csr(CSR_MATRIX_PATH)
k = 6


def k_means():
    km = KMeans(n_clusters=k, init='k-means++', max_iter=100, n_init=5, verbose=1)
    km.fit(tfs)
    return km


def t_sne():
    tfs_reduced = TruncatedSVD(n_components=k, random_state=0).fit_transform(tfs)
    tfs_embedded = TSNE(n_components=2,
                        perplexity=40, verbose=2).fit_transform(tfs_reduced)

    km = k_means()
    plt.scatter(tfs_embedded[:, 0], tfs_embedded[:, 1], marker='o', c=km.labels_, cmap='tab10', s=100, alpha=0.7)
    plt.show()

    print(davies_bouldin_score(tfs.toarray(), km.labels_))


def execute():
    t_sne()


execute()
