import constants
import nltk
import numpy as np
from gensim.models import Word2Vec
from nltk.corpus import stopwords


def tokenize_corpus(corpus):
    """
    Preprocesses the corpus.
    :param corpus: list of clean abstracts.
    :return: list of lists - each list contains all tokenized words of the corresponding abstract.
    """
    all_words = [nltk.word_tokenize(abstract) for abstract in corpus]
    for abstract_index in range(len(all_words)):
        all_words[abstract_index] = [w for w in all_words[abstract_index] if w not in stopwords.words('english')]
    return all_words


def create_word2vec_model(all_words):
    """
    Creates a word2vec model with Skip-Gram algorithm.
    :param all_words: list of lists with tokenized words.
    """
    model = Word2Vec(all_words, size=100, window=5, min_count=1, sg=1)
    model.train(all_words, total_examples=len(all_words), epochs=15)
    return model


def execute():
    corpus = list(np.load(constants.DS_CORPUS))
    all_words = tokenize_corpus(corpus)
    word2vec_model = create_word2vec_model(all_words)
    # word2vec_model.save(constants.WORD2VEC_MODEL_2)
    print(word2vec_model.wv.most_similar('cell', topn=5))


execute()
