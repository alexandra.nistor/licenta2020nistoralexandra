import constants
import json
import math
import numpy as np

all_concepts = list(np.load(constants.DS_ALL_CONCEPTS))
unique_concepts = list(np.load(constants.DS_UNIQUE_CONCEPTS))
number_of_unique_concepts = len(unique_concepts)
corpus = list(np.load(constants.DS_CORPUS))
number_of_doc = len(corpus)


def compute_tf(term, doc):
    """
    :param term.
    :param doc.
    :return: term frequency in a document.
    """
    return doc.count(term) / len(doc)


def compute_idf(term):
    """
    :param term.
    :return: inverse document frequency.
    """
    # document frequency dictionary with (key, value) (term, number of documents it occurs in)
    df_dict = {c: all_concepts.count(c) for c in all_concepts}
    df = df_dict[term]
    idf = math.log10(number_of_doc / df)
    return idf


def compute_weighting(doc, doc_index, term):
    with open(constants.DS_DICT) as fd:
        pmid_concepts_dict = json.load(fd)
    # list of lists - each list contains all concepts of the corresponding document (pmid)
    ordered_concepts = [list(set(value)) for value in pmid_concepts_dict.values()]
    s = np.load(constants.ASSOCIATION_MATRIX)
    threshold = 0.8

    tf = compute_tf(term, doc)
    i = unique_concepts.index(term)

    if tf > 0:
        idf = compute_idf(term)
        weight = tf * idf
        terms = ordered_concepts[doc_index]
        for t in terms:
            j = unique_concepts.index(t)
            score = s[i][j]
            if score > threshold and i != j:
                weight += score
    else:
        # number of the closest concepts in the entire corpus
        n = 0
        terms = unique_concepts
        score_list = []
        for t in terms:
            j = unique_concepts.index(t)
            if s[i][j] > threshold:
                if i != j:
                    n += 1
                    score_list.append(s[i][j])
        sorted_score_list = sorted(score_list, reverse=True)
        weight = 0
        for j in range(1, n + 1):
            weight += ((n - (j - 1)) / n) * sorted_score_list[j - 1]
    return weight


def build_weighting_scheme():
    ws = np.zeros((number_of_doc, number_of_unique_concepts))
    for i in range(number_of_doc):
        for j in range(number_of_unique_concepts):
            ws[i][j] = compute_weighting(corpus[i], i, unique_concepts[j])
    return ws


def execute():
    # print(compute_weighting(corpus[10], 10, unique_concepts[0]))
    ws = build_weighting_scheme()
    np.save(constants.WEIGHTING_SCHEME_1, ws)


execute()
