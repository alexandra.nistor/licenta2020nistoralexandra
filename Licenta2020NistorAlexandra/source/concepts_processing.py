import constants
import csv
import json
import numpy as np
import re
from collections import defaultdict
from nltk.corpus import stopwords


def process_concepts_csv(concepts_csv):
    """
    :param concepts_csv.
    :return: tuple - (list of all concepts, dictionary with (key, value) (pmid, list of concepts with this pmid)).
    """
    fd = open(concepts_csv, 'r')
    reader = csv.reader(fd, delimiter=',')
    next(reader)

    all_concepts = []
    pmid_concepts_dict = defaultdict(list)
    for row in reader:
        pmid = row[constants.CONCEPTS_CSV_FIELD_PMID - 1]
        concept = row[constants.CONCEPTS_CSV_FIELD_CONCEPT - 1]
        words = concept.split(' ')
        # TODO: why not .lower() the regex?
        if all(re.match('^[a-z]+$', word) and word not in stopwords.words('english') for word in words):
            all_concepts.append(concept)
            pmid_concepts_dict[pmid].append(concept)
    return all_concepts, pmid_concepts_dict


def execute():
    all_concepts, pmid_concepts_dict = process_concepts_csv(constants.CONCEPTS_CSV)

    # unique concepts in the same order as all_concepts
    unique_concepts = list({concept: 0 for concept in all_concepts})

    pmids = list(pmid_concepts_dict.keys())

    np.save(constants.DS_ALL_CONCEPTS, all_concepts)
    np.save(constants.DS_UNIQUE_CONCEPTS, unique_concepts)
    np.save(constants.DS_PMIDS, pmids)

    with open(constants.DS_DICT, 'w') as fd:
        json.dump(pmid_concepts_dict, fd)


execute()
